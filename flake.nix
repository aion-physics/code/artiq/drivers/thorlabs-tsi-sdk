{
  description = "Thorlabs Scientific Imaging SDK";

  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-21.11;

  outputs = { self, nixpkgs }:
    let
      pkgs = import nixpkgs { system = "x86_64-linux"; };
    in rec {
      packages.x86_64-linux.thorlabs-tsi = pkgs.python3Packages.buildPythonPackage rec {
        pname = "thorlabs-tsi";
        version = "2.7.1";
        src = self;
        propagatedBuildInputs = with pkgs.python3Packages; [ numpy pkgs.stdenv.cc.cc.lib ];
      };
	  
      defaultPackage.x86_64-linux = pkgs.python3.withPackages(ps: [ packages.x86_64-linux.thorlabs-tsi ]);

      devShell.x86_64-linux = pkgs.mkShell {
        name = "thorlabs-tsi-dev-shell";
        packages = [ pkgs.stdenv.cc.cc.lib ];
        buildInputs = [
          pkgs.stdenv.cc.cc.lib
          (pkgs.python3.withPackages(ps: [ packages.x86_64-linux.thorlabs-tsi ]))
        ];
        shellHook = ''
echo Entering thorlabs-tsi-dev-shell...
# fixes libstdc++ issues and libgl.so issues
LD_LIBRARY_PATH=${pkgs.stdenv.cc.cc.lib}/lib/:${self}/lib
echo LD_LIBRARY_PATH=$LD_LIBRARY_PATH
'';
      };
    };
}
